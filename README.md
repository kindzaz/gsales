## Used documentation
 *   https://docs.spring.io/spring-boot/docs/current/maven-plugin/
 *   https://www.tutorialspoint.com/spring_boot/spring_boot_application_properties.htm
 *   http://www.springboottutorial.com/spring-boot-crud-rest-service-with-jpa-hibernate
 *   https://docs.spring.io/spring-data/data-jpa/docs/current/reference/html/#jpa.query-methods.query-creation


## Build and start application using command line.
Working on: git version 2.20.1.windows.1 | Apache Maven 3.6.0 | java se 1.8.0_45-b15

 * $ git clone https://kindzaz@bitbucket.org/kindzaz/gsales.git;
 * $ cd gsales;
 * $ mvn clean install;
 * $ mvn spring-boot:run;
 * open browser, enter http://localhost:8080/
 * test: install ARC Chrome plugin. Import gsales_arc_export.json. 