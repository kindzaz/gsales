package com.girdziusas.gsales.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.girdziusas.gsales.repository.Item;
import com.girdziusas.gsales.repository.ItemRepository;

@RestController
public class GSalesController {
	private final ItemRepository itemRepository;
	
	@Autowired
	public GSalesController(final ItemRepository itemRepository) {
		this.itemRepository = itemRepository;
	}
	
	@GetMapping("/")
	public String index() {
		return "GSales. Good to see you.<br><br>"
				+ "<a href='http://localhost:8080/h2-console'>http://localhost:8080/h2-console</a>";
	}

	@PostMapping("/create")
	public ResponseEntity<Item> create(@RequestBody Item item) {
		return ResponseEntity.ok(itemRepository.saveAndFlush(item));
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Item> update(@RequestBody Item item, @PathVariable long id) {
		if (!itemRepository.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		
		item.setId(id);
		return ResponseEntity.ok(itemRepository.saveAndFlush(item));
	}
	
	@GetMapping("/get/{id}")
	public ResponseEntity<Item> get(@PathVariable long id) {
		final Optional<Item> itemOptional = itemRepository.findById(id);
		return getResponseEntity(itemOptional);
	}

	@GetMapping("/list")
	public List<Item> list() {
		return itemRepository.findAll();
	}
	
	@GetMapping("/list/{columnName}/{sortDesc}")
	public List<Item> listSort(@PathVariable String columnName, @PathVariable Boolean sortDesc) {
		Direction direction = Direction.ASC;
		if (sortDesc) {
			direction = Direction.DESC;
		}
		
		return itemRepository.findAll(new Sort(direction, columnName));
	}
	
	@GetMapping("/search/byTitle")
	public List<Item> search(@RequestParam String title) {
		return itemRepository.findByTitleContaining(title);
	}
	
	@GetMapping("/search/byPrice")
	public List<Item> search(@RequestParam Integer from, @RequestParam Integer to) {
		return itemRepository.findByPriceBetween(from, to);
	}
	
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable long id) {
		itemRepository.deleteById(id);
	}
	
	private ResponseEntity<Item> getResponseEntity(final Optional<Item> optional) {
		if (optional.isPresent()) {
			return ResponseEntity.ok(optional.get());
		}
		
		return ResponseEntity.notFound().build();
	}
}
