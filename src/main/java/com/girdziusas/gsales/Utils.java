package com.girdziusas.gsales;

import java.util.ArrayList;
import java.util.List;

import com.girdziusas.gsales.repository.Item;
import com.girdziusas.gsales.repository.Location;

public class Utils {
	private Utils() {
	}
	
	public static Item createItem(final String title, final int price, final int stock) {
		final Item item = new Item();
		item.setTitle(title);
		item.setDescription(title + "Description");
		item.setPrice(price);
		item.setStock(stock);
		item.setLocation(createLocation(title + "Country", title + "City", title + "Street", title + "GpsCoordinates"));
		return item;
	}
	
	private static Location createLocation(final String country, final String city, final String street, final String gpsCoordinates) {
		final Location location = new Location();
		location.setCountry(country);
		location.setCity(city);
		location.setStreet(street);
		location.setGpsCoordinates(gpsCoordinates);
		return location;
	}
	
	public static Iterable<Item> generateItems(final int count) {
		final List<Item> items = new ArrayList<Item>();
		for (int i = 0; i  < count; i++) {
			items.add(Utils.createItem(Integer.toHexString(i), 10000 + i, i));
		}
		
		return items;
	}
}
