package com.girdziusas.gsales.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
	
	public List<Item> findByTitleContaining(final String title);
	
	public List<Item> findByPriceBetween(final Integer from, final Integer to);
	
}
