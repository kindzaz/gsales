package com.girdziusas.gsales.repository;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Location {
	@Column(length = 32)
	private String country;
	
	@Column(length = 32)
	private String city;
	
	@Column(length = 128)
	private String street;
	
	@Column(length = 32)
	private String gpsCoordinates;

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(final String street) {
		this.street = street;
	}

	public String getGpsCoordinates() {
		return gpsCoordinates;
	}

	public void setGpsCoordinates(final String gpsCoordinates) {
		this.gpsCoordinates = gpsCoordinates;
	}

	@Override
	public String toString() {
		return String.format("Location [country=%s, city=%s, street=%s, gpsCoordinates=%s]", 
				 country, city, street, gpsCoordinates);
	}
	
	
}
