package com.girdziusas.gsales.repository;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Item {
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(length = 128, nullable = false)
	private String title;
	
	@Column(length = 256)
	private String description;
	
	private Integer price;
	
	private Integer stock;
	
	@Embedded
	private Location location;
	
	public Long getId() {
		return this.id;
	}
	
	public void setId(final Long id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(final String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(final String description) {
		this.description = description;
	}
	
	public Integer getPrice() {
		return price;
	}
	
	public void setPrice(final Integer price) {
		this.price = price;
	}
	
	public Integer getStock() {
		return stock;
	}
	
	public void setStock(final Integer stock) {
		this.stock = stock;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return String.format("Item [id=%s, title=%s, description=%s, price=%s, stock=%s, location=%s]", 
				id, title, description, price, stock, location);
	}
}
