package com.girdziusas.gsales;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.girdziusas.gsales.repository.ItemRepository;

/**
 * Spring boot starting point
 */

@SpringBootApplication
public class Application implements CommandLineRunner {
	private final ItemRepository itemRepository;
	
	@Autowired
	public Application(final ItemRepository itemRepository) {
		this.itemRepository = itemRepository;
	}
	
	public static void main(String[] args) {
		System.out.println("GSales starting.");
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		itemRepository.saveAll(Utils.generateItems(1000)); // generating initial data
		itemRepository.flush();
	}
}
